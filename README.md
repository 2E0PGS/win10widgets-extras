# win10widgets-extras.

## Designed to work as a drop in extras for http://win10widgets.com

### How to install?

* I suggest you clone the repo and then copy and paste the folders such as "Temp" etc into the "Rainmeter\Skins\Win10 Widgets" folder.

### Uptime widget

![Screenshot_71.png](https://bitbucket.org/repo/9Lgn8B/images/1967554561-Screenshot_71.png)


### Temp widget

![Screenshot_72.png](https://bitbucket.org/repo/9Lgn8B/images/2318463857-Screenshot_72.png)